#include <stdio.h>
#include "jni.h"
#include "converter_MilesToLiters.h"

#define ASM

#ifdef ASM
extern "C" {
extern double cmtl(double);
}
#else
#define cmtl(x) 3.78541178 * 100 / (x * 1.609344)
#endif

JNIEXPORT jdouble JNICALL Java_converter_MilesToLiters_convertm2l
  (JNIEnv *env, jobject obj, jdouble value) {

  return cmtl( value );

}
