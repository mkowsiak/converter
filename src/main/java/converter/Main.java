/* Main.java */ 
package converter;

public class Main {

  // Singleton - a must have
  private static Main instance = null;

  public static Main getInstance() {
    if(instance == null) {
      instance = new Main();
    }
    return instance;
  }

  private Main() {}

  public double convert(String [] args) {

    try {
      if( "miles".equals(args[0]) ) {
        return CustomConverterFactory.create(MilesToLiters.class).convert( Double.parseDouble(args[1]) );
      } else if( "liters".equals(args[0]) ) {
        return CustomConverterFactory.create(LitersToMiles.class).convert( Double.parseDouble(args[1]) );
      } else {
        return -1;
      }
    } catch(Exception ex) {
      return -1;
    }

  }

  public static void main(String[] args) {
  
    double result = Main.getInstance().convert(args);
   
    if(result == -1) {
      System.out.println("ERROR!"); 
    } else {
      System.out.println("Result: " + result); 
    }
    
  }
}
