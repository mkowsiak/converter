package converter;

public interface Converter {
  public double convert(double val);
}
