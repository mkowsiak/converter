package converter;

public class LitersToMiles implements Converter {

  static {
    System.loadLibrary("LitersToMiles");
  }

  public native double convertl2m(double val);

  public double convert(double val) {
    return convertl2m(val);
  }

}
