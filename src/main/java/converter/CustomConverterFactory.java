package converter;

public class CustomConverterFactory {

  public static Converter create(Class type) throws Exception {

    if( type.equals(MilesToLiters.class) ) {
      return new MilesToLiters();
    } else if( type.equals(LitersToMiles.class) ) {
      return new LitersToMiles();
    } else {
      throw new Exception("Incorrect conversion type");
    }
  }

}
