package converter

import spock.lang.Specification
import org.junit.Test

class LitersToMilesAndBackSpec extends Specification {

    @Test
    void " - OK, you're all set. That'll be ah, 94 dollars."() {
        given: "liters == 94"
          def liters = 94

        when: "converted to miles per galon"
          LitersToMiles ltm = new LitersToMiles();
          def result = ltm.convert(liters);

        then: "should be close to 2.50228279877"
          (result - 2.50228279877) <= 0.0000001
    }

    @Test
    void " - The Answer to the Great Question... Of Life, the Universe and Everything... Is... Forty-two"() {
        given: "miles == 42"
          def miles = 42

        when: "converted to liters per 100km"
          MilesToLiters mtl = new MilesToLiters();
          def result = mtl.convert(miles);

        then: "should be close to 5.6003472163"
          (result - 5.6003472163) <= 0.0000001
    }

}
