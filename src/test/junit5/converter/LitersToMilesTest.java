package converter;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

class LitersToMilesTest {

  @Test
  @DisplayName(" - OK, you're all set. That'll be ah, 94 dollars.")
  void convertNinetyFourLitersToMiles() {
    LitersToMiles ltm = new LitersToMiles(); 
    assertEquals(2.50228279877, ltm.convert(94), 0.0000001, "Here's 95. 94 should convert to: 2.50228279877");
  }

}
