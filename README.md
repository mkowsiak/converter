# Java based units converter

This Java based code converts miles per gallon to liters per 100km and vice versa. It is used by another project: <a href="https://gitlab.com/mkowsiak/converter-docker">converter-docker</a>.

This is heavilly over-enginered code that uses various patterns, different languages and binds together `Java` and native code.

I know that converting liters to miles and vice versa is quite simple. All you have to do is calculate

```
ltm(x) = 100 * 0.621371192 / (x * 0.264172052)
mtl(x) = 3.78541178 * 100 / (x * 1.609344)
```

But hey, why not to make it little bit more complicated.

![](./images/uml.png)

# Project structure

Source code is divided inside directories based on source language. Tests are grouped inside `tests` and divided base on testing framework.

```
.
|-- LICENSE
|-- Makefile                                         - Makefile for building shared libraries and compiling code
|-- Makefile.common                                  - Makefile with platform dependend settings
|-- Makefile.jdk.open                                - List of all modules we have make opened for Groovy
|-- Makefile.test.googletest
|-- Makefile.test.junit5
|-- Makefile.test.spock1
|-- Makefile.test.spock2
|-- README.md                                        - This README.md file
|-- images
|   `-- uml.png                                      - Beautiful architecture of the whole system
`-- src
    |-- main
    |   |-- asm
    |   |   |-- cltm.s.darwin                        - assembler code for macOS
    |   |   |-- cltm.s.linux                         - assembler code for Linux
    |   |   |-- cmtl.s
    |   |   |-- cmtl.s.darwin                        - assembler code for macOS
    |   |   `-- cmtl.s.linux                         - assembler code for Linux
    |   |-- cpp
    |   |   |-- converter_LitersToMiles.cc           - JNI based C++ code that calls assembler
    |   |   `-- converter_MilesToLiters.cc           - JNI based C++ code that calls assembler
    |   `-- java
    |       `-- converter
    |           |-- Converter.java                   - Interface of all the converters
    |           |-- CustomConverterFactory.java      - Factory class for custom converters
    |           |-- LitersToMiles.java               - Custom converter from miles to liters
    |           |-- Main.java                        - Main class
    |           `-- MilesToLiters.java               - Custom converter from liters to miles
    `-- test
        |-- cpp
        |   |-- cltm_test.cc                         - google test based tests of liters to miles converter
        |   `-- cmtl_test.cc                         - google test based tests of miles to liters converter
        |-- junit5
        |   `-- converter
        |       |-- LitersToMilesTest.java           - JUnit 5 based tests of Java code
        |       `-- MilesToLitersTest.java
        |-- spock1
        |   `-- converter
        |       `-- LitersToMilesAndBackSpec.groovy  - Spock 1 based tests of Java code
        `-- spock2
            `-- converter
                `-- LitersToMilesAndBackSpec.groovy  - Spock 2 based tests of Java code
```

# Building

    > git clone https://gitlab.com/mkowsiak/converter.git
    > make clean
    > make

# Running

    > make VALUE=${VALUE} FROM=${FROM} calculate

# Examples

    # converting 9.9 liters per 100km to miles per gallon
    > make VALUE=9.9 FROM=liters calculate

    # converting 25 miles per gallon to liters per 100km
    > make VALUE=25 FROM=miles calculate

# Running tests

You can run tests, but you will need `googletest` installed in order to compile `C++` based test cases. Running tests requires `GOOGLE_TEST` to be exported in your environment. This variable have to point at place where you can find `${GOOGLE_TEST}/include/gtest/gtest.h` and `${GOOGLE_TEST}/lib/libgtest.a`.

Once you have it in place, you can run

```
> make junit5-test-run
> make google-test-run
> make spock1-test-run
> make spock2-test-run

# alternatively you can run everything at once

> make test
```

These tests will run `JUnit` and `Spock` based tests for components developed using `Java`. There is a `googletest` based set of tests for `C++` and `assembler` based parts of the code.

# Running inside build directory

It is possible to run everything inside `build` directory instead of main directory of the projects. You simply have to call `make -f ../Makefile`.

```
# Building the project
> mkdir build
> cd build
> make -f ../Makefile clean
> make -f ../Makefile

# Running all the tests
> make -f ../Makefile test

# converting 9.9 liters per 100km to miles per gallon
> make -f ../Makefile VALUE=9.9 FROM=liters calculate

# converting 25 miles per gallon to liters per 100km
> make -f ../Makefile VALUE=25 FROM=miles calculate
```

# Known limitations

At the moment error handling is not quite efficient. This project is used only inside <a href="https://gitlab.com/mkowsiak/converter-docker">converter-docker</a>.

# Java code used for converting

`converter-docker` is based on simple `Java` code that is based on singleton patter with factory pattern applied to native code wrappers. Calculations are performed by `C` based code executed via `JNI`. This code calls routines developed in `assembler`.

You can find source code of `converter-docker` here: <a href="https://gitlab.com/mkowsiak/converter-docker">converter-docker</a>.

