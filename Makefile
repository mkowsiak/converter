location      := $(CURDIR)/$(lastword $(MAKEFILE_LIST))
location.dir  := $(dir $(location))
Makefile.main := true
result        := $(call assert,$(GOOGLE_TEST),GOOGLE_TEST is not defined)

all: build-all

check-env:
ifndef GOOGLE_TEST
	$(error GOOGLE_TEST variable is undefined - it is not possible to compile googletest based codes)
endif

# Common variables
include $(location.dir)/Makefile.common

# Each kind of tests is defined in separate Makefile
include $(location.dir)/Makefile.test.junit5
include $(location.dir)/Makefile.test.googletest
include $(location.dir)/Makefile.test.spock1
include $(location.dir)/Makefile.test.spock2

build-all: compile-c

create-dirs:
	-mkdir -p jar
	-mkdir -p obj
	-mkdir -p lib
	-mkdir -p $(Spock1.Target)
	-mkdir -p $(Spock2.Target)
	-mkdir -p $(JUnit5.Target)

compile-java: create-dirs
	@$(JAVA_HOME)/bin/javac -h $(Cpp.SRC) -d target $(Java.SRC)/converter/*.java

compile-c: compile-java
	as -c -o obj/cltm.o $(Asm.SRC)/cltm.s.$(ARCH)
	as -c -o obj/cmtl.o $(Asm.SRC)/cmtl.s.$(ARCH)
	g++ -g -shared -fpic -I${JAVA_HOME}/include -I${JAVA_HOME}/include/$(ARCH) $(Cpp.SRC)/converter_LitersToMiles.cc obj/cltm.o -o lib/libLitersToMiles.$(EXT)
	g++ -g -shared -fpic -I${JAVA_HOME}/include -I${JAVA_HOME}/include/$(ARCH) $(Cpp.SRC)/converter_MilesToLiters.cc obj/cmtl.o -o lib/libMilesToLiters.$(EXT)

test: check-env junit5-test-run google-test-run spock1-test-run spock2-test-run

calculate:
	@$(JAVA_HOME)/bin/java -Djava.library.path=$(LD_LIBRARY_PATH):./lib -cp target converter.Main $(FROM) $(VALUE)

clean:
	@-rm -rfv target
	@-rm $(Cpp.SRC)/*.h
	@-rm -rfv obj
	@-rm -rfv lib
	@-rm -rf jar
